.. Horkos documentation master file, created by
   sphinx-quickstart on Sat Oct 24 17:56:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Horkos
==================================

**Horkos** is a library for validating data at the edges of data systems.

Installation
------------

Horkos is on PyPI_ so simply run:

.. code-block:: console

    $ pip install horkos

Quick Start
-----------

   >>> import horkos
   >>> from horkos import types
   >>> from horkos import checks
   >>> schema = horkos.Schema(fields=[
   ...     horkos.Field(
   ...         'method',
   ...         types.String,
   ...         checks=[checks.Choice(['GET', 'POST', 'PUT', 'DELETE'])],
   ...     ),
   ...     horkos.Field('path', types.String),
   ...     horkos.Field('response_code', types.Integer),
   ... ])
   >>> schema.process({
   ...     'method': 'GET',
   ...     'path': '/my-settings',
   ...     'response_code': '200',
   ... })
   {'method': 'GET', 'path': '/my-settings', 'response_code': 200}
   >>> schema.process({
   ...     'method': 'NOT-AN-OPTION',
   ...     'path': '/my-settings',
   ...     'response_code': 200,
   ... })
   Traceback (most recent call last):
     File "<stdin>", line 1, in <module>
     File "/home/kjschiroo/gitlab/kjschiroo/horkos/horkos/_schemaomatic.py", line 142, in process
       self._apply_checks(cast)
     File "/home/kjschiroo/gitlab/kjschiroo/horkos/horkos/_schemaomatic.py", line 107, in _apply_checks
       raise errors.RecordValidationError(msg)
   horkos.errors.RecordValidationError: Checks failed: value of "NOT-AN-OPTION" for method did not pass choice check


Yaml Schemas
------------
Horkos provides a yaml data documentation format that can be used to create
a schema. This has the advantage of pairing documentation with functionality,
making both more effective.

First declare your data schema in a yaml file. This documents the data
as well as makes claims about its field types and properties.

.. literalinclude:: sample.yaml
   :language: yaml

This file is loaded into a schema that can be used to process and validate
records.

   >>> import horkos
   >>> schema = horkos.load_schema('http_requests.yaml')
   >>> record = {
   ...     'path': '/my-settings',
   ...     'params': None,
   ...     'method': 'GET',
   ...     'response_code': 200,
   ...     'timestamp': '2020-10-25T02:22:16'
   ... }
   >>> schema.process(record)
   {'path': '/my-settings', 'params': None, 'method': 'GET', 'response_code': 200, 'timestamp': '2020-07-27T15:23:45'}
   >>> record['timestamp'] = 'Sat Oct 24 2020 21:22:16 GMT-0500'
   >>> schema.process(record)
   Traceback (most recent call last):
     File "<stdin>", line 1, in <module>
     File "/horkos/_schemaomatic.py", line 47, in process
       f'Check errors - {", ".join(error_set)}'
   horkos.errors.RecordValidationError: Check errors - value of "Sat Oct 24 2020 21:22:16 GMT-0500" in timestamp did not pass iso_timestamp check


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Schemas <schemas>
   Checks <checks>
   Types <types>
   API <api>


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. _PyPI: https://pypi.org/project/horkos
