Types
=======

.. note::
  Casting in :code:`horkos` works a bit differently than in pure python.
  :code:`horkos` acknowledges the ubiquity of CSVs as a data exchange format
  and has casting designed to tolerate common CSV encoding of types while
  still providing validation of type compatibility. In order to support
  this :code:`horkos` provides more selective casting compared to python's more
  aggressive casting.

  As an example :code:`bool('False') == True` in python, but would be cast
  to :code:`False` for a boolean field in :code:`horkos`. This
  also means that certain otherwise valid values in python will be rejected
  in :code:`horkos`. As an example :code:`bool(42) == True`, but
  would be rejected in :code:`horkos` since :code:`42` is a non-standard way
  of encoding :code:`True`.

**boolean:**
  A value that is either true or false. :code:`boolean` fields will cast
  strings that case insensitively match :code:`"true"` or :code:`"false"`
  exactly to :code:`True` and :code:`False` respectively. It will cast
  integers :code:`0` and :code:`1` to :code:`False` and :code:`True`
  respectively.

**float:**
  A numerical value with decimal components. :code:`float` fields will cast
  integers, as well as float strings and integer strings to float values.

**integer:**
  A whole number value. :code:`integer` fields will cast integer strings
  to integers, they will also cast float values to integers if there is no
  decimal component. Float values with decimal components will fail to
  cast to integers.

**string:**
  Any string value. :code:`horkos` will only cast values that can be cast
  unambiguously to a string such as integers and floats. Other values
  that have a string encoding in python will not be cast to a string if
  there isn't exactly one reasonable string encoding. Examples of this are
  booleans (since :code:`True`, :code:`TRUE`, and :code:`true` are all equally
  reasonable) or dictionaries (since :code:`{'foo': 'bar'}` and
  :code:`{"foo": "bar"}` are equally reasonable).
