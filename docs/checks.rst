Checks
===============

Checks are used to validate data within a field while it is being processed.

Built-in
--------
:code:`horkos` provides several built-in checks.

**between:**
  Confirm that the given value is between two other values. Accepts
  :code:`upper` and :code:`lower` specifying the upper and lower limits
  for values to be between.

  .. code-block:: yaml

    checks:
    - name: between
      args:
        upper: upper_value
        lower: lower_value

**choice:**
  Confirm that the given value is in a list of valid options.
  Accepts :code:`options` argument listing all valid choices.

  .. code-block:: yaml

    checks:
    - name: choice
      args:
        options:
        - option_1
        - option_2

**email:**
  Confirm that the given value is a valid email address.

  .. code-block:: yaml

    checks:
    - email

**iso_timestamp:**
  Confirm that the given value is a valid iso timestamp. A valid example
  is :code:`2020-10-25T20:27:21Z`.

  .. code-block:: yaml

    checks:
    - iso_timestamp

**json:**
  Confirm that the given value is a valid JSON string. Accepts no
  arguments.

  .. code-block:: yaml

    checks:
    - json

**json_array:**
  Confirm that the given value is a valid JSON string that can be deserialized
  to an array. Accepts no arguments.

  .. code-block:: yaml

    checks:
    - json_array

**json_object:**
  Confirm that the given value is a valid JSON string that can be deserialized
  to an object. Accepts no arguments.

  .. code-block:: yaml

    checks:
    - json_object

**maximum:**
  Confirm that the given value is less than the specified limit.
  Accepts :code:`limit` argument specifying the maximum value and a
  :code:`inclusive` boolean to determine if values matching the
  maximum should be allowed.

  .. code-block:: yaml

    checks:
    - name: maximum
      args:
        limit: max_value
        inclusive: true  # defaults to true

**maximum_length:**
  Confirm that the given value is of a length less than or equal to the
  specified limit. Accepts :code:`limit` argument specifying the maximum
  length a string is allowed to be.

  .. code-block:: yaml

    checks:
    - name: maximum_length
      args:
        limit: max_length

**minimum:**
  Confirm that the given value is greater than the specified limit.
  Accepts :code:`limit` argument specifying the minimum value and a
  :code:`inclusive` boolean to determine if values matching the
  minimum should be allowed.

  .. code-block:: yaml

    checks:
    - name: minimum
      args:
        limit: max_value
        inclusive: true  # defaults to true

**regex:**
  Confirm that the given value matches a specific regex.
  Accepts :code:`regex` argument specifying the pattern values
  must match and an optional :code:`ignore_case` boolean to
  determine if case should be ignored when matching the regex.

  .. code-block:: yaml

    checks:
    - name: regex
      args:
        regex: pattern_to_match
        ignore_case: false  # defaults to false

**uuid:**
  Confirm that the given value is a valid uuid.

  .. code-block:: yaml

    checks:
    - uuid
